const createList = (arr, parent) => {
    if (!parent) {
        parent = document.body
    }
    const list = document.createElement('ul')
    for (let i = 0; i < arr.length; i++) {
        const li = document.createElement('li')
        li.innerText = arr[i]
        list.appendChild(li)

    }
    parent.appendChild(list)

}
const a = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
createList(a)
const b = ["1", "2", "3", "sea", "user", 23];
const parent = document.getElementById('example')
createList(b, parent)
const createListH = (arr, parent) => {
    if (!parent) {
        parent = document.body
    }
    const list = document.createElement('ul')
    arr.map((el) => {
        const liOut = document.createElement('li')
        const current = el
        if (Array.isArray(current)) {
            createListH(current, liOut)
            list.appendChild(liOut)

        } else {
            liOut.innerText = current
            list.appendChild(liOut)
        }
    })
    
    parent.appendChild(list)
}
const c = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
createListH (c)

const t = document.getElementById('timer')
let count = 3
const timer = setInterval(() => {
    t.innerText = count--
    if (count === 0) {
        document.body.innerHTML = ''
    clearInterval(timer)
    }
}, 1000)